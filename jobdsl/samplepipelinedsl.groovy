// job('NodeJS Docker pipeline') {
    // wrappers {
    //     nodejs('nodejs') // this is the name of the NodeJS installation in 
    //                      // Manage Jenkins -> Configure Tools -> NodeJS Installations -> Name
    // }
    // steps {
        pipelineJob('node pipeline example') {

            def repo = 'https://gitlab.com/gerwel_uy/jenkins-docker.git'

            triggers {
                scm('H/5 * * * *')
            }
            definition {
                cpsScm {
                    scm {
                        git {
                            remote {url(repo)}
                            branches('master','**/feature*')
                            scriptPath('jenkins-pipeline/misc/Jenkinsfile')
                            
            }
        }
        
    }
}
    }
    // }
// }
